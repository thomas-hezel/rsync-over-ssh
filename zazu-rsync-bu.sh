bash#!/bin/bash

# avoidng collisions with other rsync processes

#the minutes passed to the script 21h*60min = 1260 Minuten
minutes=1260

# Make sure no one else is using rsync
pro_on=$(ps aux | grep -c rsync)

# if someone is using rsync
# grep is also producing one entry so -gt 1
# alternativ would be "grep -c rsyn[c]"

while [ $pro_on -gt 1 ]
do
  sleep 60
  ((minutes--))

  # we are close to the next day no need to start
  if [ $minutes -lt 60 ] ; then
    exit 0
  fi

  pro_on=$(ps aux | grep -c rsync)
done


# Simple backup with rsync
# local-mode, tossh-mode, fromssh-mode

SOURCES=(/Volumes/raid/ )
TARGET="/srv/backup"

# edit or comment with "#"
MONTHROTATE=monthrotate                 # use DD instead of YYMMDD
RSYNCCONF=(--delete --xattrs --acls --exclude=/Volumes/raid/.DocumentRevisions-V100 --exclude=/Volumes/raid/.TemporaryItems --exclude=/Volumes/raid/.Trashes --exclude=/Volumes/raid/.apdisc)
MAILREC="webmaster@zazu.berlin.please.change"

SSHUSER="backupuser"
FROMSSH="backupuser@999.888.77.66"
#TOSSH="tossh-server"
SSHPORT=2222

### a lot of zazu edits ###

MOUNT="/bin/mount"; FGREP="/bin/fgrep"; SSH="/usr/bin/ssh"
LN="/bin/ln"; ECHO="/bin/echo"; DATE="/bin/date"; RM="/bin/rm"
DPKG="/usr/bin/dpkg"; AWK="/usr/bin/awk"; MAIL="/usr/bin/mail"
CUT="/usr/bin/cut"; TR="/usr/bin/tr"; RSYNC="/usr/bin/rsync"
LAST="last"; INC="--link-dest=$TARGET/$LAST"; LS="/bin/ls"

#zazu log all days in one file
all_logprint() {
    echo -e "\n $*" >> all-days-${0}.log
}

LOG=$0.log
$DATE > $LOG

if [ "${TARGET:${#TARGET}-1:1}" != "/" ]; then
  TARGET=$TARGET/
fi

#zazu check whether today is the same number as the symlink "last" to the numbered folder
#also no 2 backups on one day - gives an ERROR
HEUTE=$($DATE +%d)
cd $TARGET
LAST_SYMLINK=$($LS -l | grep ^l | grep -o "[0-9][0-9]$")
cd /home/backupuser/bin

if [ $HEUTE -ne $LAST_SYMLINK ]; then

  if [ -z "$MONTHROTATE" ]; then
    TODAY=$($DATE +%y%m%d)
  else
    TODAY=$($DATE +%d)
  fi

  if [ "$SSHUSER" ] && [ "$SSHPORT" ]; then
    S="$SSH -p $SSHPORT -l $SSHUSER";
  fi

  for SOURCE in "${SOURCES[@]}"
    do
      if [ "$S" ] && [ "$FROMSSH" ] && [ -z "$TOSSH" ]; then
        $ECHO "$RSYNC -e \"$S\" -avR \"$FROMSSH:$SOURCE\" ${RSYNCCONF[@]} $TARGET$TODAY $INC"  >> $LOG

        #log only errors "2>> $LOG" since logging all files backuped will crash the mailer, original ">> $LOG 2>&1"
        $RSYNC -e "$S" -avR "$FROMSSH:$SOURCE" "${RSYNCCONF[@]}" "$TARGET"$TODAY $INC 2>> $LOG
        if [ $? -ne 0 ]; then
          ERROR=1
        fi
      fi
      if [ "$S" ]  && [ "$TOSSH" ] && [ -z "$FROMSSH" ]; then
        $ECHO "$RSYNC -e \"$S\" -avR \"$SOURCE\" ${RSYNCCONF[@]} \"$TOSSH:$TARGET$TODAY\" $INC " >> $LOG
        $RSYNC -e "$S" -avR "$SOURCE" "${RSYNCCONF[@]}" "$TOSSH:\"$TARGET\"$TODAY" $INC >> $LOG 2>&1
        if [ $? -ne 0 ]; then
          ERROR=1
        fi
      fi
      if [ -z "$S" ]; then
        $ECHO "$RSYNC -avR \"$SOURCE\" ${RSYNCCONF[@]} $TARGET$TODAY $INC"  >> $LOG
        $RSYNC -avR "$SOURCE" "${RSYNCCONF[@]}" "$TARGET"$TODAY $INC  >> $LOG 2>&1
        if [ $? -ne 0 ]; then
          ERROR=1
        fi
      fi
  done

  if [ "$S" ] && [ "$TOSSH" ] && [ -z "$FROMSSH" ]; then
    $ECHO "$SSH -p $SSHPORT -l $SSHUSER $TOSSH $LN -nsf $TARGET$TODAY $TARGET$LAST" >> $LOG
    $SSH -p $SSHPORT -l $SSHUSER $TOSSH "$LN -nsf \"$TARGET\"$TODAY \"$TARGET\"$LAST" >> $LOG 2>&1
    if [ $? -ne 0 ]; then
      ERROR=1
    fi
  fi

  ## zazu added [ -z "$ERROR" ], no symlink if error

  if ( [ "$S" ] && [ "$FROMSSH" ] && [ -z "$TOSSH" ] && [ -z "$ERROR" ] ) || ( [ -z "$S" ] );  then
    $ECHO "$LN -nsf $TARGET$TODAY $TARGET$LAST" >> $LOG
    $LN -nsf "$TARGET"$TODAY "$TARGET"$LAST  >> $LOG 2>&1
    if [ $? -ne 0 ]; then
      ERROR=1
    fi
  fi

else
  $ECHO "Since one month no backup or only with ERROR (todays day of the month and day of last backup have the same number), kept the last backup from day ${TODAY}, didn't overwirte the folder. No backup today, next backup is tomorrow." >> $LOG
  ERROR=1
fi

$DATE >> $LOG

if [ -n "$MAILREC" ]; then
  if [ $ERROR ];then
    $MAIL -s "Error Backup $LOG" $MAILREC < $LOG
    #zazu all log
    all_logprint "ERROR _ _ _ _ _ _ _ _ _ _ _ \n" "$(< $LOG)"
  else
    $MAIL -s "Backup $LOG" $MAILREC < $LOG
    #zazu all log
    all_logprint "$(< $LOG)"
  fi
fi